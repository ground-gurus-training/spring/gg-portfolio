package com.groundgurus.ggportfolio.dao;

import com.groundgurus.ggportfolio.model.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SkillDao extends JpaRepository<Skill, Long> {

}
