package com.groundgurus.ggportfolio.dao;

import com.groundgurus.ggportfolio.model.Portfolio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PortfolioDao extends JpaRepository<Portfolio, String> {

}
