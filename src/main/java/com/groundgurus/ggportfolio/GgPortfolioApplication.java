package com.groundgurus.ggportfolio;

import com.groundgurus.ggportfolio.dao.PortfolioDao;
import com.groundgurus.ggportfolio.dao.SkillDao;
import com.groundgurus.ggportfolio.model.Portfolio;
import com.groundgurus.ggportfolio.model.Skill;
import java.time.LocalDate;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
public class GgPortfolioApplication extends SpringBootServletInitializer {

  public static void main(String[] args) {
    SpringApplication.run(GgPortfolioApplication.class, args);
  }

  @Bean
  @Transactional
  @Profile("dev")
  public CommandLineRunner commandLineRunner(PortfolioDao portfolioDao, SkillDao skillDao) {
    return args -> {
      // remove existing data in db
      portfolioDao.deleteAll();

      // create the skills
      Skill html5 = skillDao.save(html5 = Skill.builder()
          .name("HTML5")
          .percentage(100)
          .build());

      Skill css3 = skillDao.save(Skill.builder()
          .name("CSS3")
          .percentage(50)
          .build());

      Skill javaScript = skillDao.save(Skill.builder()
          .name("JavaScript")
          .percentage(90)
          .build());

      // create the portfolio
      Portfolio portfolio = Portfolio.builder()
          .id("portfolio-12345")
          .firstName("Philip Mark")
          .lastName("Gutierrez")
          .age(getMyAge())
          .jobTitle("Full Stack Developer")
          .email("philip@groundgurus.com")
          .skype("pgutierrez@skype.com")
          .phone("+629271234567")
          .address("Metro Manila")
          .status("Available")
          .skills(List.of(html5, css3, javaScript))
          .build();

      portfolioDao.save(portfolio);
    };
  }

  private int getMyAge() {
    LocalDate today = LocalDate.now();
    return today.getYear() - 1988;
  }
}
